====================
chkboot-desktopalert
====================

------------------------------------------
notify changes detected in your boot files
------------------------------------------

:Author: Ju <ju@heisec.de>, Giancarlo Razzolini <grazzolini@gmail.com>,
         Chris Warner <inhies@gmail.com> and Kevin MacMartin <prurigro@gmail.com>
:Date: 2019-04-17
:Copyright: GPL-2
:Version: 1.3
:Manual section: 8
:Manual group: System Manager's Manual

SYNOPSIS
========

``chkboot-desktopalert``

DESCRIPTION
===========

Display a notification with a detailed diff of added, changes and removed files,
detected by ``chkboot``.

It will also display a notification when the MBR has been modified.

**You will need to add this program to your desktop environment's autostart.**

SEE ALSO
========

* ``man chkboot`` and ``man chkboot-check``
